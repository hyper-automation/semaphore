package com.semaphore.controller;

import com.semaphore.SemaphoreApplicationTests;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.net.URI;
import java.net.URISyntaxException;

@RunWith(SpringRunner.class)
@SpringBootTest
class SemaphoreControllerTest extends SemaphoreApplicationTests {

    @Autowired
    SemaphoreController semaphoreController ;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void  getCallTest()throws Exception{
        String payload = getMockPayload();
        String response=semaphoreController.getCall(payload);
        Assert.assertEquals("Issue data attachments are processed successfully",response);

    }
    @Test
    void getCall() throws Exception {
        String payload = getMockPayload();

        String response=semaphoreController.getCall(payload);
        final String baseUrl = "http://20.198.72.26:" + 8085 + "/getCall/";
        URI uri = new URI(baseUrl);
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth("admin_hyperautomation@jktech.com","123456");

        HttpEntity request = new HttpEntity<>(payload, headers);

        try {
            ResponseEntity responseEntity=restTemplate.postForEntity(uri, request, String.class);
            Assert.assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            //Verify bad request and missing header
            Assert.assertEquals(400, ex.getRawStatusCode());
            Assert.assertEquals(true, ex.getResponseBodyAsString().contains("Missing request header"));
        }


    }


    private String getMockPayload() {

        return "{\n" +
                "\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/10150\",\n" +
                "\t\"id\": 10150,\n" +
                "\t\"key\": \"TEST-1\",\n" +
                "\t\"changelog\": {\n" +
                "\t\t\"startAt\": 0,\n" +
                "\t\t\"maxResults\": 0,\n" +
                "\t\t\"total\": 0,\n" +
                "\t\t\"histories\": null\n" +
                "\t},\n" +
                "\t\"fields\": {\n" +
                "\t\t\"statuscategorychangedate\": \"2021-08-02T19:03:01.334+0900\",\n" +
                "\t\t\"issuetype\": {\n" +
                "\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/issuetype/10005\",\n" +
                "\t\t\t\"id\": 10005,\n" +
                "\t\t\t\"description\": \"Stories track functionality or features expressed as user goals.\",\n" +
                "\t\t\t\"iconUrl\": \"https://ha-jkt.atlassian.net/secure/viewavatar?size=medium&avatarId=10315&avatarType=issuetype\",\n" +
                "\t\t\t\"name\": \"Story\",\n" +
                "\t\t\t\"subtask\": false,\n" +
                "\t\t\t\"fields\": null,\n" +
                "\t\t\t\"statuses\": [],\n" +
                "\t\t\t\"namedValue\": \"Story\"\n" +
                "\t\t},\n" +
                "\t\t\"timespent\": null,\n" +
                "\t\t\"customfield_10030\": [],\n" +
                "\t\t\"customfield_10031\": null,\n" +
                "\t\t\"project\": {\n" +
                "\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/project/10003\",\n" +
                "\t\t\t\"id\": 10003,\n" +
                "\t\t\t\"key\": \"TEST\",\n" +
                "\t\t\t\"name\": \"Test\",\n" +
                "\t\t\t\"description\": null,\n" +
                "\t\t\t\"avatarUrls\": {\n" +
                "\t\t\t\t\"48x48\": \"https://ha-jkt.atlassian.net/secure/projectavatar?pid=10003&avatarId=10404\",\n" +
                "\t\t\t\t\"24x24\": \"https://ha-jkt.atlassian.net/secure/projectavatar?size=small&s=small&pid=10003&avatarId=10404\",\n" +
                "\t\t\t\t\"16x16\": \"https://ha-jkt.atlassian.net/secure/projectavatar?size=xsmall&s=xsmall&pid=10003&avatarId=10404\",\n" +
                "\t\t\t\t\"32x32\": \"https://ha-jkt.atlassian.net/secure/projectavatar?size=medium&s=medium&pid=10003&avatarId=10404\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"issuetypes\": null,\n" +
                "\t\t\t\"projectCategory\": null,\n" +
                "\t\t\t\"email\": null,\n" +
                "\t\t\t\"lead\": null,\n" +
                "\t\t\t\"components\": null,\n" +
                "\t\t\t\"versions\": null,\n" +
                "\t\t\t\"projectTypeKey\": \"software\",\n" +
                "\t\t\t\"simplified\": true\n" +
                "\t\t},\n" +
                "\t\t\"customfield_10032\": null,\n" +
                "\t\t\"fixVersions\": [],\n" +
                "\t\t\"aggregatetimespent\": null,\n" +
                "\t\t\"resolution\": {\n" +
                "\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/resolution/10000\",\n" +
                "\t\t\t\"id\": 10000,\n" +
                "\t\t\t\"name\": \"Done\",\n" +
                "\t\t\t\"description\": \"Work has been completed on this issue.\",\n" +
                "\t\t\t\"namedValue\": \"Done\"\n" +
                "\t\t},\n" +
                "\t\t\"customfield_10029\": null,\n" +
                "\t\t\"resolutiondate\": 1627898581329,\n" +
                "\t\t\"workratio\": -1,\n" +
                "\t\t\"watches\": {\n" +
                "\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/issue/TEST-1/watchers\",\n" +
                "\t\t\t\"watchCount\": 1,\n" +
                "\t\t\t\"isWatching\": false\n" +
                "\t\t},\n" +
                "\t\t\"lastViewed\": \"2021-08-02T19:02:58.354+0900\",\n" +
                "\t\t\"issuerestriction\": {\n" +
                "\t\t\t\"issuerestrictions\": {},\n" +
                "\t\t\t\"shouldDisplay\": true\n" +
                "\t\t},\n" +
                "\t\t\"created\": 1621860458619,\n" +
                "\t\t\"customfield_10020\": [{\n" +
                "\t\t\t\"id\": 6,\n" +
                "\t\t\t\"name\": \"TEST Sprint 1\",\n" +
                "\t\t\t\"state\": \"active\",\n" +
                "\t\t\t\"boardId\": 4,\n" +
                "\t\t\t\"goal\": \"\",\n" +
                "\t\t\t\"startDate\": \"2021-05-24T12:52:45.308Z\",\n" +
                "\t\t\t\"endDate\": \"2021-06-07T12:52:40.000Z\"\n" +
                "\t\t}],\n" +
                "\t\t\"customfield_10021\": null,\n" +
                "\t\t\"customfield_10022\": null,\n" +
                "\t\t\"priority\": {\n" +
                "\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/priority/3\",\n" +
                "\t\t\t\"id\": 3,\n" +
                "\t\t\t\"name\": \"Medium\",\n" +
                "\t\t\t\"iconUrl\": \"https://ha-jkt.atlassian.net/images/icons/priorities/medium.svg\",\n" +
                "\t\t\t\"namedValue\": \"Medium\"\n" +
                "\t\t},\n" +
                "\t\t\"customfield_10023\": null,\n" +
                "\t\t\"customfield_10024\": null,\n" +
                "\t\t\"customfield_10025\": \"10004_*:*_1_*:*_74384_*|*_10006_*:*_31_*:*_602507449_*|*_10005_*:*_30_*:*_5435540905\",\n" +
                "\t\t\"labels\": [],\n" +
                "\t\t\"customfield_10016\": null,\n" +
                "\t\t\"customfield_10017\": null,\n" +
                "\t\t\"customfield_10018\": {\n" +
                "\t\t\t\"hasEpicLinkFieldDependency\": false,\n" +
                "\t\t\t\"showField\": false,\n" +
                "\t\t\t\"nonEditableReason\": {\n" +
                "\t\t\t\t\"reason\": \"PLUGIN_LICENSE_ERROR\",\n" +
                "\t\t\t\t\"message\": \"The Parent Link is only available to Jira Premium users.\"\n" +
                "\t\t\t}\n" +
                "\t\t},\n" +
                "\t\t\"customfield_10019\": \"0|i000xa:x\",\n" +
                "\t\t\"aggregatetimeoriginalestimate\": null,\n" +
                "\t\t\"timeestimate\": null,\n" +
                "\t\t\"versions\": [],\n" +
                "\t\t\"issuelinks\": [],\n" +
                "\t\t\"assignee\": {\n" +
                "\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/user?accountId=602261616180010069a54689\",\n" +
                "\t\t\t\"name\": null,\n" +
                "\t\t\t\"key\": null,\n" +
                "\t\t\t\"accountId\": \"602261616180010069a54689\",\n" +
                "\t\t\t\"emailAddress\": null,\n" +
                "\t\t\t\"avatarUrls\": {\n" +
                "\t\t\t\t\"48x48\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\",\n" +
                "\t\t\t\t\"24x24\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\",\n" +
                "\t\t\t\t\"16x16\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\",\n" +
                "\t\t\t\t\"32x32\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"displayName\": \"Chandipriya-jktech\",\n" +
                "\t\t\t\"active\": true,\n" +
                "\t\t\t\"timeZone\": \"Asia/Tokyo\",\n" +
                "\t\t\t\"groups\": null,\n" +
                "\t\t\t\"locale\": null,\n" +
                "\t\t\t\"accountType\": \"atlassian\"\n" +
                "\t\t},\n" +
                "\t\t\"updated\": 1627898581333,\n" +
                "\t\t\"status\": {\n" +
                "\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/status/10006\",\n" +
                "\t\t\t\"description\": \"\",\n" +
                "\t\t\t\"iconUrl\": \"https://ha-jkt.atlassian.net/\",\n" +
                "\t\t\t\"name\": \"Done\",\n" +
                "\t\t\t\"untranslatedName\": null,\n" +
                "\t\t\t\"id\": 10006,\n" +
                "\t\t\t\"statusCategory\": {\n" +
                "\t\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/statuscategory/3\",\n" +
                "\t\t\t\t\"id\": 3,\n" +
                "\t\t\t\t\"key\": \"done\",\n" +
                "\t\t\t\t\"colorName\": \"green\",\n" +
                "\t\t\t\t\"name\": \"Complete\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"untranslatedNameValue\": null\n" +
                "\t\t},\n" +
                "\t\t\"components\": [],\n" +
                "\t\t\"timeoriginalestimate\": null,\n" +
                "\t\t\"description\": null,\n" +
                "\t\t\"customfield_10010\": null,\n" +
                "\t\t\"customfield_10014\": null,\n" +
                "\t\t\"timetracking\": {\n" +
                "\t\t\t\"originalEstimate\": null,\n" +
                "\t\t\t\"remainingEstimate\": null,\n" +
                "\t\t\t\"timeSpent\": null,\n" +
                "\t\t\t\"originalEstimateSeconds\": 0,\n" +
                "\t\t\t\"remainingEstimateSeconds\": 0,\n" +
                "\t\t\t\"timeSpentSeconds\": 0\n" +
                "\t\t},\n" +
                "\t\t\"customfield_10015\": null,\n" +
                "\t\t\"customfield_10005\": null,\n" +
                "\t\t\"customfield_10006\": null,\n" +
                "\t\t\"security\": null,\n" +
                "\t\t\"customfield_10007\": null,\n" +
                "\t\t\"customfield_10008\": null,\n" +
                "\t\t\"aggregatetimeestimate\": null,\n" +
                "\t\t\"customfield_10009\": null,\n" +
                "\t\t\"attachment\": [{\n" +
                "\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/attachment/10002\",\n" +
                "\t\t\t\"id\": 10002,\n" +
                "\t\t\t\"filename\": \"Copy of ACCORD_FORM_Vs_CLAIM_APPLICATION.xlsx\",\n" +
                "\t\t\t\"author\": {\n" +
                "\t\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/user?accountId=602261616180010069a54689\",\n" +
                "\t\t\t\t\"name\": null,\n" +
                "\t\t\t\t\"key\": null,\n" +
                "\t\t\t\t\"accountId\": \"602261616180010069a54689\",\n" +
                "\t\t\t\t\"emailAddress\": null,\n" +
                "\t\t\t\t\"avatarUrls\": {\n" +
                "\t\t\t\t\t\"48x48\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\",\n" +
                "\t\t\t\t\t\"24x24\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\",\n" +
                "\t\t\t\t\t\"16x16\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\",\n" +
                "\t\t\t\t\t\"32x32\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\"\n" +
                "\t\t\t\t},\n" +
                "\t\t\t\t\"displayName\": \"Chandipriya-jktech\",\n" +
                "\t\t\t\t\"active\": true,\n" +
                "\t\t\t\t\"timeZone\": \"Asia/Tokyo\",\n" +
                "\t\t\t\t\"groups\": null,\n" +
                "\t\t\t\t\"locale\": null,\n" +
                "\t\t\t\t\"accountType\": \"atlassian\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"created\": 1622462242424,\n" +
                "\t\t\t\"size\": 53649,\n" +
                "\t\t\t\"mimeType\": \"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\",\n" +
                "\t\t\t\"content\": \"https://ha-jkt.atlassian.net/secure/attachment/10002/Copy+of+ACCORD_FORM_Vs_CLAIM_APPLICATION.xlsx\"\n" +
                "\t\t}],\n" +
                "\t\t\"summary\": \"Create Conditions\",\n" +
                "\t\t\"creator\": {\n" +
                "\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/user?accountId=602261616180010069a54689\",\n" +
                "\t\t\t\"name\": null,\n" +
                "\t\t\t\"key\": null,\n" +
                "\t\t\t\"accountId\": \"602261616180010069a54689\",\n" +
                "\t\t\t\"emailAddress\": null,\n" +
                "\t\t\t\"avatarUrls\": {\n" +
                "\t\t\t\t\"48x48\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\",\n" +
                "\t\t\t\t\"24x24\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\",\n" +
                "\t\t\t\t\"16x16\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\",\n" +
                "\t\t\t\t\"32x32\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"displayName\": \"Chandipriya-jktech\",\n" +
                "\t\t\t\"active\": true,\n" +
                "\t\t\t\"timeZone\": \"Asia/Tokyo\",\n" +
                "\t\t\t\"groups\": null,\n" +
                "\t\t\t\"locale\": null,\n" +
                "\t\t\t\"accountType\": \"atlassian\"\n" +
                "\t\t},\n" +
                "\t\t\"subtasks\": [],\n" +
                "\t\t\"reporter\": {\n" +
                "\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/user?accountId=602261616180010069a54689\",\n" +
                "\t\t\t\"name\": null,\n" +
                "\t\t\t\"key\": null,\n" +
                "\t\t\t\"accountId\": \"602261616180010069a54689\",\n" +
                "\t\t\t\"emailAddress\": null,\n" +
                "\t\t\t\"avatarUrls\": {\n" +
                "\t\t\t\t\"48x48\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\",\n" +
                "\t\t\t\t\"24x24\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\",\n" +
                "\t\t\t\t\"16x16\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\",\n" +
                "\t\t\t\t\"32x32\": \"https://secure.gravatar.com/avatar/5a2cc424479b748de0f0825f0892dc80?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FC-2.png\"\n" +
                "\t\t\t},\n" +
                "\t\t\t\"displayName\": \"Chandipriya-jktech\",\n" +
                "\t\t\t\"active\": true,\n" +
                "\t\t\t\"timeZone\": \"Asia/Tokyo\",\n" +
                "\t\t\t\"groups\": null,\n" +
                "\t\t\t\"locale\": null,\n" +
                "\t\t\t\"accountType\": \"atlassian\"\n" +
                "\t\t},\n" +
                "\t\t\"aggregateprogress\": {\n" +
                "\t\t\t\"progress\": 0,\n" +
                "\t\t\t\"total\": 0\n" +
                "\t\t},\n" +
                "\t\t\"customfield_10000\": \"{}\",\n" +
                "\t\t\"customfield_10001\": null,\n" +
                "\t\t\"customfield_10002\": null,\n" +
                "\t\t\"customfield_10003\": null,\n" +
                "\t\t\"customfield_10004\": null,\n" +
                "\t\t\"environment\": null,\n" +
                "\t\t\"duedate\": null,\n" +
                "\t\t\"progress\": {\n" +
                "\t\t\t\"progress\": 0,\n" +
                "\t\t\t\"total\": 0\n" +
                "\t\t},\n" +
                "\t\t\"votes\": {\n" +
                "\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/issue/TEST-1/votes\",\n" +
                "\t\t\t\"votes\": 0,\n" +
                "\t\t\t\"hasVoted\": false\n" +
                "\t\t},\n" +
                "\t\t\"comment\": {\n" +
                "\t\t\t\"maxResults\": 0,\n" +
                "\t\t\t\"total\": 0,\n" +
                "\t\t\t\"startAt\": 0,\n" +
                "\t\t\t\"comments\": [],\n" +
                "\t\t\t\"last\": false\n" +
                "\t\t},\n" +
                "\t\t\"worklog\": {\n" +
                "\t\t\t\"maxResults\": 20,\n" +
                "\t\t\t\"total\": 0,\n" +
                "\t\t\t\"startAt\": 0,\n" +
                "\t\t\t\"worklogs\": [],\n" +
                "\t\t\t\"last\": false\n" +
                "\t\t}\n" +
                "\t},\n" +
                "\t\"renderedFields\": null\n" +
                "}";
    }
}