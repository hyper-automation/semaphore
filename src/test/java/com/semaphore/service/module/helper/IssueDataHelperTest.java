package com.semaphore.service.module.helper;

import com.semaphore.SemaphoreApplicationTests;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class IssueDataHelperTest extends SemaphoreApplicationTests {
    IssueDataHelper issueDataHelper;
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void setIssueData() throws JSONException {
        issueDataHelper=new IssueDataHelper();
        String jsonString=getMockPayload();
        JSONObject jsonObject=new JSONObject(jsonString);
        String issueId = jsonObject.get("id").toString();
        issueDataHelper.SetIssueData(new JSONObject(),jsonObject,issueId);
    }
    private String getMockPayload() {
long val=123L;
        return "{\n" +
                "  \"id\":\"123\",\n" +
                "\t\"fields\": {\n" +
                "\t\t\"issuetype\": {\n" +
                "\t\t\t\"name\": \"Story\"\n" +
                "\t\t},\n" +
                "\t\t\"project\": {\n" +
                "\t\t\t\"name\": \"Test\",\n" +
                "\t\t\t\"description\": null\n" +
                "\t\t},\n" +
                "\t\t\"resolution\": {\n" +
                "\t\t\t\"name\": \"Done\",\n" +
                "\t\t\t\"description\": \"Work has been completed on this issue.\"\n" +
                "\t\t},\n" +
                "\t\t\"priority\": {\n" +
                "\t\t\t\"name\": \"Medium\"\n" +
                "\t\t},\n" +
                "\t\t\"assignee\": {\n" +
                "\t\t\t\"displayName\": \"Chandipriya-jktech\"\n" +
                "\t\t},\n" +
                "\t\t\"status\": {\n" +
                "\t\t\t\"name\": \"Done\"\n" +
                "\t\t},\n" +
                "\t\t\"description\": null,\n" +
                "\t\t\"attachment\": [{\n" +
                "\t\t\t\"self\": \"https://ha-jkt.atlassian.net/rest/api/2/attachment/10002\",\n" +
                "\n" +
                "\t\t\t\"filename\": \"Copy of ACCORD_FORM_Vs_CLAIM_APPLICATION.xlsx\",\n" +
                "\n" +
                "\t\t\t\"displayName\": \"Chandipriya-jktech\",\n" +
                "\t\t\t\"content\": \"https://ha-jkt.atlassian.net/secure/attachment/10002/Copy+of+ACCORD_FORM_Vs_CLAIM_APPLICATION.xlsx\"\n" +
                "\t\t}],\n" +
                "\t\t\"summary\": \"Create Conditions\",\n" +
                "\t\t\"comment\": {\n" +
                "\t\t\t\"comments\": [{\n" +
                "\t\t\t\t\"updated\": "+val+",\n" +
                "\t\t\t\t\"body\": \"test\"\n" +
                "\t\t\t}]\n" +
                "\t\t}\n" +
                "\n" +
                "\t}\n" +
                "}";
    }
}