package com.semaphore.util;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.json.CDL;
import org.json.JSONArray;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

@Slf4j
public class UtilDTO {
    public InputStream convertToCsv(JSONArray jsonArray){
        String csv = CDL.toString(jsonArray);
        InputStream stream = new ByteArrayInputStream(csv.getBytes
                (Charset.forName("UTF-8")));
        return stream;
    }
    public String connectFtp(InputStream stream, String issueId, String fileName) {
        String server = "hamiddleware.centralindia.cloudapp.azure.com";
        String user = "newftpuser";
        String password = "^&XW8ARccie76";
        int port = 21;
        FTPClient ftpClient = new FTPClient();
        String dirToCreate = "/home/newftpuser/ftp/upload/semaphore/" + issueId;
        String filePath = "/home/newftpuser/ftp/upload/semaphore/" + issueId + "/" + fileName;
        try {
            ftpClient.connect(server, port);
            ftpClient.login(user, password);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            ftpClient.makeDirectory(dirToCreate);
            log.info("Start uploading file");
            boolean done = ftpClient.storeFile(filePath, stream);
            stream.close();
            if (done) {
                log.info("File is uploaded successfully.");
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return filePath;
    }

}
