package com.semaphore.controller;

import com.semaphore.service.SemaphoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SemaphoreController {
    @Autowired
    SemaphoreService service;
    @CrossOrigin(origins = "*")
    @PostMapping("/getCall")
    public String getCall(@RequestBody String requestPayload) throws Exception {

        return service.getCall(requestPayload);
    }
    @CrossOrigin(origins = "*")
    @PostMapping("/getIssueData")
    public String getIssueData(@RequestBody String requestPayload) throws Exception {
        return service.getIssueData(requestPayload);
    }
    @CrossOrigin(origins = "*")
    @PostMapping("/getAttachmentData")
    public String getAttachmentData(@RequestBody String requestPayload) throws Exception {
        return service.getAttachmentData(requestPayload);

    }
    @CrossOrigin(origins = "*")
    @PostMapping("/getDataCall/{fileName}")
    public String getDataCall(@PathVariable String fileName, @RequestBody byte[] requestPayload) throws Exception {
        byte[] bytes=requestPayload;
        System.out.println(fileName);
        return fileName;
    }

}
