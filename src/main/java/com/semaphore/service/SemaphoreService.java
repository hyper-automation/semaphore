package com.semaphore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.io.InputStream;

@Service
public class SemaphoreService {

    @Autowired
    SemaphoreServiceHelper serviceHelper;

    public String getCall(String jsonString){
        return serviceHelper.getCall(jsonString);
    }
    public String getAttachmentData(String payload){
        return serviceHelper.getAttachmentData(payload);
    }
    public String getIssueData(String payload){
        return serviceHelper.getIssueData(payload);
    }
}
