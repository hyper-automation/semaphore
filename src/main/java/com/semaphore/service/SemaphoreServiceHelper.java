package com.semaphore.service;

import com.semaphore.service.module.helper.IssueDataHelper;
import com.semaphore.util.UtilDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


@Component
@Slf4j
public class SemaphoreServiceHelper {

    @Autowired
    RestTemplate restTemplate;



    UtilDTO utilDTO;

    public String getCall(String jsonString) {
        try {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject(jsonString);
        JSONObject issueData = new JSONObject();
        utilDTO = new UtilDTO();
        String issueId = jsonObject.get("id").toString();
        String fileName = issueId + ".csv";
        IssueDataHelper issueDataHelper=new IssueDataHelper();
        issueDataHelper.SetIssueData(issueData, jsonObject, issueId);
        jsonArray.put(issueData);
        InputStream stream = utilDTO.convertToCsv(jsonArray);
        String filePath = utilDTO.connectFtp(stream, issueId, fileName);
        log.info("IssueData FileName:"+fileName + "/n,IssueData FilePath:" + filePath);
        processAttachments(jsonObject, issueId);
        return "Issue data attachments are processed successfully";
        }catch (Exception e){
            log.error(e.getMessage(),e);
            return e.getMessage();
        }

    }

    public void processAttachments(JSONObject jsonObject, String issueId) throws IOException {

        JSONArray attachementArray = jsonObject.getJSONObject("fields").getJSONArray("attachment");
        for (int i = 0; i < attachementArray.length(); i++) {
            JSONObject attachment = attachementArray.getJSONObject(i);
            String fileContentUrl = attachment.getString("content");
            String fileName = attachment.getString("filename");
            log.info("FileName:" + fileName);
            log.info("FileContentUrl:" + fileContentUrl);
            processContent(fileContentUrl, fileName, issueId);
        }

    }

    public void processContent(String content, String fileName, String issueId) throws IOException {
        utilDTO = new UtilDTO();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBasicAuth("chandipriya.gangishetty@jktech.com", "5BPxt7Tjd6CgRlQcKmdVC5CD");
        HttpEntity request = new HttpEntity(httpHeaders);
        InputStream inputStream = restTemplate.exchange(content, HttpMethod.GET, request, Resource.class).getBody().getInputStream();
        String filePath = utilDTO.connectFtp(inputStream, issueId, fileName);
        log.info("FTPFilePath:" + filePath);
    }
    public String getAttachmentData(String payload)  {
        try {
            utilDTO = new UtilDTO();
            log.info(payload);
            String username = "admin";
            String password = "Ch@ndi897";
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setBasicAuth(username, password);
            HttpEntity request = new HttpEntity(httpHeaders);
            String attchmentJson= restTemplate.exchange(payload, HttpMethod.GET, request, String.class).getBody();
            String downloadurl = new JSONObject(attchmentJson).getJSONObject("result").getString("download_link");
            String fileName = new JSONObject(attchmentJson).getJSONObject("result").getString("file_name");
            log.info(fileName+" : "+downloadurl);
            InputStream inputStream= restTemplate.exchange(downloadurl, HttpMethod.GET, request, Resource.class).getBody().getInputStream();
            byte[] bytes=IOUtils.toByteArray(inputStream);
            log.info(String.valueOf(bytes.length));
            HttpHeaders httpHeaders1=new HttpHeaders();
            httpHeaders1.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            httpHeaders1.setBasicAuth("admin_hyperautomation@jktech.com","123456");
            HttpEntity request2 = new HttpEntity(bytes,httpHeaders1);
            String res=restTemplate.exchange("http://localhost:61264/api/values?FileName="+fileName, HttpMethod.POST, request2, String.class).getBody();
            return res;
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return "";
    }
    public String getIssueData(String payload){
        log.info(payload);
        return "issue response";
    }

}
