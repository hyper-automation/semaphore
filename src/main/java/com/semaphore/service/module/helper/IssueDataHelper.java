package com.semaphore.service.module.helper;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;

@Slf4j
public class IssueDataHelper {
    public void SetIssueData(JSONObject issueData,JSONObject jsonObject,String issueId){
        JSONObject fieldsObject=jsonObject.getJSONObject("fields");
        JSONArray attachmentArray = fieldsObject.getJSONArray("attachment");
        issueData.put("id", issueId);
        issueData.put("issueType", fieldsObject.getJSONObject("issuetype").get("name"));
        issueData.put("summary", fieldsObject.getString("summary"));
        issueData.put("priority", fieldsObject.getJSONObject("priority").get("name"));
        issueData.put("status", fieldsObject.getJSONObject("status").get("name"));
        issueData.put("assignee", fieldsObject.getJSONObject("assignee").getString("displayName"));
        issueData.put("project", fieldsObject.getJSONObject("project").getString("name"));
        issueData.put("resolution_status", fieldsObject.getJSONObject("resolution").getString("name"));
        issueData.put("resolution_decription", fieldsObject.getJSONObject("resolution").getString("description"));
        issueData.put("issue_description", fieldsObject.get("description").toString());
        issueData.put("Attachments", attachmentArray.length());
        JSONArray commentsArray = fieldsObject.getJSONObject("comment").getJSONArray("comments");
        JSONArray comments = new JSONArray();
        for (int i = 0; i < commentsArray.length(); i++) {
            JSONObject comment = new JSONObject();

            Date updatedDate = new Date(commentsArray.getJSONObject(i).getLong("updated"));
            comment.put("updated", updatedDate);

            comment.put("body", commentsArray.getJSONObject(i).getString("body"));
            comments.put(comment);
        }
        issueData.put("comments", comments.toString());
        log.info("IssueData:"+issueData.toString());

    }
}
